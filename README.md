# Git

- Présentation https://pad.oslandia.net/p/wcX_2OF0d

- Présentation PDF: [slide.pdf](slide.pdf)


- Mail: thomas.muguet@oslandia.com

- Lien vers questionnaire d'évaluation : https://framaforms.org/evaluation-git-1709913089

## Configuration git

`git config --global user.name "Thomas Muguet"`

`git config --global user.email "thomas.muguet@oslandia.com"`

## Eviter de taper son mot de passe à chaque fois

`git config --global credential.helper store`

`git pull`

